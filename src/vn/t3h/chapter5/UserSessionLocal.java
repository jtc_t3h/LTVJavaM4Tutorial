package vn.t3h.chapter5;

import java.util.List;

import javax.ejb.Local;

@Local
public interface UserSessionLocal {

	public List<UserEntity> findAll();
	
	public UserEntity findById(Long id);
	
	public int insert(UserEntity userEntity);
}
