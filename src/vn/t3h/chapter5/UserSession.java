package vn.t3h.chapter5;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Session Bean implementation class UserSession
 */
@Stateless
@LocalBean
public class UserSession implements UserSessionRemote, UserSessionLocal {

    /**
     * Default constructor. 
     */
    public UserSession() {
        // TODO Auto-generated constructor stub
    }
    
    //DI
    @PersistenceContext(unitName="LTVJavaM4Tutorial")
    private EntityManager em;

	@Override
	public List<UserEntity> findAll() {
		Query query = em.createNativeQuery("select * from User");
		
		return query.getResultList();
	}

	@Override
	public int insert(UserEntity userEntity) {
		em.persist(userEntity);
		return 1;
	}

	@Override
	public UserEntity findById(Long id) {
		return em.getReference(UserEntity.class, id);
	}

}
