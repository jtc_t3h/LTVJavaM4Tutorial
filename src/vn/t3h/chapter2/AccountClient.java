package vn.t3h.chapter2;

import java.util.Properties;
import java.util.Random;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class AccountClient implements Runnable {

	private static String PKG_INTERFACES = "org.jboss.ejb.client.naming";
//	private static String name = "ejb:/LTVJavaM4Tutorial/AccountSessionBean!vn.t3h.chapter2.AccountSessionBeanRemote?stateful";
	private static String name = "ejb:/LTVJavaM4Tutorial/AccountSessionBean!vn.t3h.chapter2.AccountSessionBeanRemote";
	
	public static void main(String[] args) {
		// Tao 10 thread (10 client)
		AccountClient client = new AccountClient();
		for (int i = 1; i <= 10; i++) {
			Thread thread = new Thread(client, "T" + i);
			thread.start();
		}
	}

	@Override
	public void run() {
		Properties properties = new Properties();
		properties.put(Context.URL_PKG_PREFIXES, PKG_INTERFACES);
		try {
			InitialContext context = new InitialContext(properties);
			AccountSessionBeanRemote bean = (AccountSessionBeanRemote)context.lookup(name);
			
			bean.setName(Thread.currentThread().getName());
			bean.deposite(Math.random() * 1000);
			
			System.out.println(Thread.currentThread().getName() + "---->" + bean.getName());
			System.out.println(Thread.currentThread().getName() + "---->" + bean.getInfo());
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
	}

}
