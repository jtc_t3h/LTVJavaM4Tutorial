package vn.t3h.chapter2;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Bai1_HelloWorldTest {

	private static String PKG_INTERFACES = "org.jboss.ejb.client.naming";
	private static String name = "ejb:/LTVJavaM4Tutorial/Bai1_HelloWorldBean!vn.t3h.chapter2.Bai1_HelloWorld";
	
	/**
	 * @param args
	 * @throws NamingException 
	 */
	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		Properties properties = new Properties();
		properties.put(Context.URL_PKG_PREFIXES, PKG_INTERFACES);
		
		InitialContext context = new InitialContext(properties);
		Bai1_HelloWorld bean = (Bai1_HelloWorld) context.lookup(name);

		System.out.println(bean.sayHello());
	}

}
