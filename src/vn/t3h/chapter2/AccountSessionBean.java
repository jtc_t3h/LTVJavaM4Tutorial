package vn.t3h.chapter2;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class AccountSessionBean
 */
@Stateless
@LocalBean
public class AccountSessionBean implements AccountSessionBeanRemote, AccountSessionBeanLocal {

	private String name;
	private double balance = 0;
	
    /**
     * Default constructor. 
     */
    public AccountSessionBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void deposite(double amount) {
		balance += amount;
	}

	@Override
	public void withdraw(double amount) {
		balance -= amount;
	}

	@Override
	public double getBalance() {
		return balance;
	}

	@Override
	public String getInfo() {
		return "[" + name + "]- balance: " + balance;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
	
	

}
