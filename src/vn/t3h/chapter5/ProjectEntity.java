package vn.t3h.chapter5;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "project")
public class ProjectEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 200)
	private String name;

	@Column(name = "sta_date")
	private Timestamp startDate;

	@Column(name = "end_date")
	private Timestamp endDate;

	@ManyToMany(mappedBy = "listOfProject")
	private List<EmployeeEntity> employeeEntities;

	@ManyToMany(mappedBy = "projectEntities")
	private List<DepartmentEntity> departmentEntities;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public List<EmployeeEntity> getEmployeeEntities() {
		return employeeEntities;
	}

	public void setEmployeeEntities(List<EmployeeEntity> employeeEntities) {
		this.employeeEntities = employeeEntities;
	}

	public List<DepartmentEntity> getDepartmentEntities() {
		return departmentEntities;
	}

	public void setDepartmentEntities(List<DepartmentEntity> departmentEntities) {
		this.departmentEntities = departmentEntities;
	}

}
