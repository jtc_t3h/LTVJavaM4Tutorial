package vn.t3h.chapter1.ejb;

import javax.ejb.Stateless;

@Stateless
public interface Hello {

	public String sayHello(String name);
}
