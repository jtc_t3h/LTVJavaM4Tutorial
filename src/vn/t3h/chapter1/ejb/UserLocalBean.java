package vn.t3h.chapter1.ejb;

import java.util.List;

import javax.ejb.Stateless;

@Stateless
public interface UserLocalBean {
	public int insert(User user);
	
	public List<User> findAll();
}
