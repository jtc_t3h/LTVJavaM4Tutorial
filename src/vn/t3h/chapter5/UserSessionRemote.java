package vn.t3h.chapter5;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface UserSessionRemote {

	public List<UserEntity> findAll();
	public int insert(UserEntity userEntity);
	public UserEntity findById(Long id);
}
