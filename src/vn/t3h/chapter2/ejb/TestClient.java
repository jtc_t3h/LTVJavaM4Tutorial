package vn.t3h.chapter2.ejb;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class TestClient {

	private static String PKG_INTERFACES = "org.jboss.ejb.client.naming";
	private static String name = "ejb:/LTVJavaM4Tutorial/CounterImpl!vn.t3h.chapter2.ejb.CountRemoteBean";

	public static void main(String[] args) throws NamingException {
		Properties properties = new Properties();
		properties.put(Context.URL_PKG_PREFIXES, PKG_INTERFACES);

		InitialContext context = new InitialContext(properties);
		CountRemoteBean bean = (CountRemoteBean) context.lookup(name);

		System.out.println(bean.add(1, 2));
	}

}
