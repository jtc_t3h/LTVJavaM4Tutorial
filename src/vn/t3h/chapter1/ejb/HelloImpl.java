package vn.t3h.chapter1.ejb;

import javax.ejb.Stateless;

@Stateless
public class HelloImpl implements Hello {

	@Override
	public String sayHello(String name) {
		return "Hello " + name;
	}

}
