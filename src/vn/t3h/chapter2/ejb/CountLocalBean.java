package vn.t3h.chapter2.ejb;

import javax.ejb.Local;

@Local
public interface CountLocalBean {

	public int add(int x, int y);
}
