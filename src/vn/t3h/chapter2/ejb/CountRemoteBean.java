package vn.t3h.chapter2.ejb;

import javax.ejb.Remote;

@Remote
public interface CountRemoteBean {

	public int add(int x, int y);
	
}
