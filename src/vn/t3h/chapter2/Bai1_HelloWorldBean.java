package vn.t3h.chapter2;

import javax.ejb.Stateless;

/**
 * Session Bean implementation class HelloWorldBean
 */
@Stateless
public class Bai1_HelloWorldBean implements Bai1_HelloWorld {

    /**
     * Default constructor. 
     */
    public Bai1_HelloWorldBean() {
        // TODO Auto-generated constructor stub
    }

    public String sayHello(){
    	return "Hello World, EJB Project!";
    }

}
