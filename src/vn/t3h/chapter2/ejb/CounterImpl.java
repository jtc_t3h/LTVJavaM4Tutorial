package vn.t3h.chapter2.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

@Stateful
public class CounterImpl implements CountLocalBean, CountRemoteBean {

	public CounterImpl() {
		System.out.println("Default constructor.");
	}
	
	@PostConstruct
	public void abc() {
		System.out.println("PostContruc lifecycle callback method.");
	}
	
	@Override
	public int add(int x, int y) {
		return x + y;
	}

}
