package vn.t3h.chapter2;

import javax.ejb.Local;

@Local
public interface AccountSessionBeanLocal {

	public void deposite(double amount);
	public void withdraw(double amount);
	public double getBalance();
	public String getInfo();
	public void setName(String name);
	public String getName();
}
