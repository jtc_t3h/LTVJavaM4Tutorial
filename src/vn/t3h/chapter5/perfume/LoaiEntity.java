package vn.t3h.chapter5.perfume;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
@Table(name="loai")
public class LoaiEntity implements Serializable {

	
	private int id;
	private String name;

	@Id // property-based\
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="tenloai", length=200)
	public String getName() {
		return name;
	}

	public void setName(String tenloai) {
		this.name = tenloai;
	}

}
